/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is reponsible for implementing the core graphics view that serves as the calculator display. However wasn't fully implemented! Needs some work. 
 */

#import "CalculatorDisplay.h"
@import CoreText;

@implementation CalculatorDisplay

@synthesize displayOperandString;
@synthesize displayExpression;

- (id)initWithFrame:(CGRect)frame
{
    NSLog(@"initWithFrame");
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    NSLog(@"initWithCoder");
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        self.clearsContextBeforeDrawing = YES;
        self.displayOperandString = @"0";
        self.displayExpression = [[NSMutableArray alloc]init]; 
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    /* Fonts and Text Color */
    UIFont *normalFont = [UIFont systemFontOfSize:15];
    //UIFont *fractionFont = [UIFont systemFontOfSize:20];
    
    UIColor * textColor = [UIColor colorWithRed:0.1215 green:0.1215 blue:0.149 alpha:1.0];
    
    /* Variables needed */
    float startX = 100;
    float startY = 50;
    float height = 100;
    //float offset = 10;
    float width = 20;
    
    /* Get a context first */
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    /* Set the pen line thickness */
    CGContextSetLineWidth (context, 1.0);
    
    /* Set the rectangle filling Color: White */
    CGContextSetRGBFillColor(context, 4.0, 0.0, 0.0, 1.0);
    
    /* If there is no displayExpression array elements */
    if (displayExpression.count == 0)
    {

    }
    /* If there is elements in displayExpression array */
    else{
        /* Create Rectangle */
        CGRect displayOperandRectangle = CGRectMake(startX, startY, width, height);
        [displayOperandString drawInRect: displayOperandRectangle withAttributes:@{NSFontAttributeName:normalFont, NSForegroundColorAttributeName:textColor}];
        CGContextFillRect(context, displayOperandRectangle);
        CGContextStrokePath (context);
    }
    
}


@end

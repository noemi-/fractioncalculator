/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is reponsible for defining a stack
 */

#import <Foundation/Foundation.h>

@interface Stack : NSObject

@property (nonatomic, assign, readonly) unsigned long count;
@property (strong, nonatomic) NSMutableArray * aStack;

- (void)push:(id)item;
- (id)pop;
- (id)top;
-(BOOL)isEmpty; 

@end

/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is responsible for defining the calculator brain that is performing individual operations or an entire expression.
 */

#import "ArithmeticProtocol.h"
#import <Foundation/Foundation.h>
#import "FractionalNumber.h"

@interface CalculatorBrain : NSObject

@property (nonatomic, strong) id<ArithmeticProtocol, NSObject> waitingOperand;
@property (assign, nonatomic) SEL waitingOperator;
@property (nonatomic) id<ArithmeticProtocol, NSObject> incomingOperand;

+(CalculatorBrain *) calculatorBrain;
-(id<ArithmeticProtocol, NSObject>)send:(id<ArithmeticProtocol, NSObject>)operand to:(SEL)operation;
-(id<ArithmeticProtocol, NSObject>)evaluate:(NSMutableArray *)anExpression;
@end

/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is reponsible for implementing a stack
 */

#import "Stack.h"

@implementation Stack

@synthesize aStack;


-(id)init
{
    self = [super init];
    if (self)
    {
        aStack = [[NSMutableArray alloc]init];
    }
    
    return self;
}

-(unsigned long)count{
    return aStack.count;
}

-(void)push:(id)item
{
    if (item != nil)
    {
        [aStack addObject:item];
    }
}

-(id)pop
{
    if (aStack.count > 0)
    {
        id PoppedItem = [aStack objectAtIndex:aStack.count-1];
        [aStack removeLastObject];
        return PoppedItem;
        
    }
    return nil;
}

-(id)top
{
    if (aStack.count > 0)
    {
        return [aStack objectAtIndex:aStack.count-1];
    }
    else
        return nil;
}

-(BOOL)isEmpty
{
    return aStack.count == 0; 
}

@end

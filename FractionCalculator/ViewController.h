//
//  ViewController.h
//  FractionCalculator
//
//  Created by Noemi Quezada on 10/19/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalculatorDisplay.h"


@interface ViewController : UIViewController

@property (nonatomic) NSString * operandString;
@property (nonatomic) NSMutableArray * expression;
@property (nonatomic) NSMutableArray * stringExpression;
@property (nonatomic) NSMutableArray * displayExpression;
@property (nonatomic) NSString * displayOperandString; 
@property (weak, nonatomic) IBOutlet UILabel *calculatorDisplay;



- (IBAction)buttonPressed:(id)sender;


@end


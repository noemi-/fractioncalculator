/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is responsible for defining the InfixPostfix Machine which takes an infix expression given as an array and turns it into postfix expression returned as an array
 */

#import <Foundation/Foundation.h>

@interface InfixPostfixMachine : NSObject

+(InfixPostfixMachine *)infixPostfixMachine; 
-(NSMutableArray * )postfix:(NSMutableArray *)anExpression; 

@end

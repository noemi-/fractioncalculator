/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is reponsible for defining the core graphics view for the calculator display
 */

#import <UIKit/UIKit.h>

@interface CalculatorDisplay : UIView

@property (strong, nonatomic)NSMutableArray * displayExpression; /* Display Expression */
@property (strong, nonatomic)NSString * displayOperandString; /* Display String */

@end

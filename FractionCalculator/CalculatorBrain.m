/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is responsible for implementing the calculator brain that is performing individual operations or an entire expression.
 */

#import "CalculatorBrain.h"
#import "Operator.h"
#import "InfixPostfixMachine.h"
#import "Stack.h"

static CalculatorBrain * _calculatorBrain;

@implementation CalculatorBrain

@synthesize waitingOperand;
@synthesize waitingOperator;
@synthesize incomingOperand;

-(id)init
{
    self = [super init];
    if (self)
    {
        waitingOperand = [[FractionalNumber alloc]initWithNumerator:0 denominator:1]; 
        waitingOperator = @selector(add:);
    }
    
    return self;
}

+(CalculatorBrain *)calculatorBrain
{
    if (_calculatorBrain == nil)
    {
        _calculatorBrain = [[CalculatorBrain alloc]init];
    }
    return _calculatorBrain;
}

-(id<ArithmeticProtocol, NSObject>)send:(id<ArithmeticProtocol, NSObject>)operand to:(SEL)operation
{
    waitingOperand = [waitingOperand performSelector:(SEL)operation withObject:operand];
    
    return waitingOperand;
}



-(id<ArithmeticProtocol, NSObject>)evaluate:(NSMutableArray *)anExpression
{
    id<ArithmeticProtocol, NSObject> result = nil;
    
    /* Call the IPMachine */
    InfixPostfixMachine * ipMachine = [InfixPostfixMachine infixPostfixMachine];
    NSMutableArray * postfixExpression = [[NSMutableArray alloc]init];
    postfixExpression = [ipMachine postfix:anExpression];
    Stack * evaluationStack = [[Stack alloc]init];
    
    /* Evaluate the postfix Expression */ 
    if (postfixExpression.count != 0)
    {
        if (postfixExpression.count == 1)
        {
            return postfixExpression[0]; 
        }
        for (id item in postfixExpression)
        {
            if ([item isKindOfClass:[Operator class]])
            {
                incomingOperand = [evaluationStack pop];
                waitingOperand = [evaluationStack pop];
                Operator * temp = [[Operator alloc]init];
                temp = item;
                result = [self send:incomingOperand to:temp.arithmeticOperation];
                [evaluationStack push:result];
            }
            else{
                [evaluationStack push:item];
            }
        }
        return result;

    }
    return result;
}


@end

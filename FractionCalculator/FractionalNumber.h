/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is responsible for defining the FractionalNumber object that does arithmetic operations with fractions
 */

#import "ArithmeticProtocol.h"
#import <Foundation/Foundation.h>

@interface FractionalNumber : NSObject <ArithmeticProtocol, NSObject>

@property (nonatomic, assign) int numerator;

@property (nonatomic, assign) int denominator;


-(FractionalNumber *) initWithNumerator:(int)aNumerator denominator:(int)aDenominator;

//-(FractionalNumber *) add:(FractionalNumber *) rightHandSide;
//
//-(FractionalNumber *) subtract:(FractionalNumber *) rightHandSide;

//-(FractionalNumber *) multiply: (FractionalNumber *) rightHandSide;
//
//-(FractionalNumber *) divide: (FractionalNumber *) rightHandSide;

-(FractionalNumber *) plusOrMinus;

//-(NSString *) stringRepresentationOfFraction;



/* Utitlity functions */
-(void)crossReduce: (FractionalNumber *)rightHandside;

-(void)reduceFraction;

+(int)greatestCommonDivisor: (int)firstNumber and: (int)secondNumber;

+(int)leastCommonMultiple: (int)firstNumber and: (int)secondNumber;


@end

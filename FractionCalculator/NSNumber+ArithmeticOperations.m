/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is a building block file that implements a NSNumber Category that does Arithmetic Operations
 */

#import "NSNumber+ArithmeticOperations.h"

@implementation NSNumber (ArithmeticOperations)

-(NSString * )stringRepresentationOfFraction
{
    /* Not fully implemented */ 
    return @"";
}

-(NSNumber *)add:(NSNumber *)rightHandSide
{
    return [[NSNumber alloc]initWithDouble:(self.doubleValue + rightHandSide.doubleValue)];
    
}

-(NSNumber *) subtract:(NSNumber *)rightHandSide
{
    return [[NSNumber alloc]initWithDouble:(self.doubleValue - rightHandSide.doubleValue)];
}

-(NSNumber *) multiply: (NSNumber *) rightHandSize
{
    return [[NSNumber alloc]initWithDouble:(self.doubleValue * rightHandSize.doubleValue)];
}

-(NSNumber *)divide: (NSNumber *) rightHandSize
{
    return [[NSNumber alloc]initWithDouble:(self.doubleValue / rightHandSize.doubleValue)];
}

@end

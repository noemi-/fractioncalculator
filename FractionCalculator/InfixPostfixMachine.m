/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is responsible for implementing the InfixPostfix Machine which takes an infix expression given as an array and turns it into postfix expression returned as an array
 */

#import "InfixPostfixMachine.h"
#import "Stack.h"
#import "Queue.h"
#import "Operator.h"

static InfixPostfixMachine * _infixPostfixMachine; 

@implementation InfixPostfixMachine

+(InfixPostfixMachine *)infixPostfixMachine
{
    if (_infixPostfixMachine == nil)
    {
        _infixPostfixMachine = [[InfixPostfixMachine alloc]init];
    }
    return _infixPostfixMachine;
}


-(NSMutableArray *)postfix:(NSMutableArray *)anExpression
{
    Queue * outputQueue = [[Queue alloc]init];
    Stack * operatorStack = [[Stack alloc]init];
    
    
    for (id item in anExpression)
    {
        if ([item isKindOfClass:[Operator class]])
        {
            Operator * tempExpression = item;
            
            if (operatorStack.isEmpty)
            {
                [operatorStack push: tempExpression];
            }
            else{
                Operator * top = [operatorStack top];
                if (tempExpression.precedence >= top.precedence)
                {
                    while (tempExpression.precedence >= top.precedence)
                    {
                        id poppedItem = [operatorStack pop];
                        [outputQueue enqueue:poppedItem];
                        if (!operatorStack.isEmpty)
                        {
                            top = [operatorStack top];
                        }
                        else{
                            break;
                        }
                    }
                    
                    [operatorStack push: tempExpression];
                        
                        
                }
                else{
                    [operatorStack push:tempExpression];
                }
            }
        }
        else{
            [outputQueue enqueue:item];
        }
    }
    
    while (!operatorStack.isEmpty)
    {
        id temp = [operatorStack pop];
        [outputQueue enqueue:temp];
    }
    
    NSMutableArray * postfixExpression = [outputQueue morphToArray];
    
    return postfixExpression;

}

@end

//
//  AppDelegate.h
//  FractionCalculator
//
//  Created by Noemi Quezada on 10/19/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


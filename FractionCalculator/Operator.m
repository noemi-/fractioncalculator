/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is reponsible for implementing the operators methods
 */

#import "Operator.h"

@implementation Operator

@synthesize precedence;
@synthesize arithmeticOperation;

-(NSString *)description
{
    return [NSString stringWithFormat:@"Precedence: %ld ", self.precedence];
}

@end

/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is an Arithmetic Protocol that defines methods of this protocol that does arithmetic operations and representing string as a fraction 
 */

#import <Foundation/Foundation.h>

@protocol ArithmeticProtocol <NSObject>

-(id) add: (id)rightHandSide;
-(id) subtract: (id) rightHandSide;
-(id) multiply: (id) rightHandSize;
-(id) divide: (id) rightHandSize;

/* Return as a string */
-(NSString *) stringRepresentationOfFraction;

@end

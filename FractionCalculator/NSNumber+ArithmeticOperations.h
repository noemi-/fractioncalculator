/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is a building block file that defines a NSNumber Category that does Arithmetic Operations
 */

#import "ArithmeticProtocol.h"
#import <Foundation/Foundation.h>

@interface NSNumber (ArithmeticOperations) <ArithmeticProtocol>

//-(NSNumber *) add: (NSNumber *) rhs;
//-(NSNumber *) subtract: (NSNumber *) rhs;
//-(NSNumber *) multiply: (NSNumber *) rhs;
//-(NSNumber *) divide: (NSNumber *) rhs;

@end

/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is reponsible for defining a queue
 */

#import <Foundation/Foundation.h>

@interface Queue : NSObject

@property (nonatomic, assign, readonly) unsigned long count;
@property (strong, nonatomic) NSMutableArray * aQueue;

- (void)enqueue:(id)item;
- (id)dequeue;
- (id)top;
-(BOOL)isEmpty;

-(NSMutableArray *)morphToArray; 

@end

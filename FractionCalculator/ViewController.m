//
//  ViewController.m
//  FractionCalculator
//
//  Created by Noemi Quezada on 10/19/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "CalculatorBrain.h"
#import "ViewController.h"
#import "FractionalNumber.h"
#import "Operator.h"

/* Operation Look up Table */
static NSDictionary * operationSelectorLookUpTable;
static NSDictionary * operationStringLookUpTable;

@interface ViewController ()

@property (assign, nonatomic) BOOL isOperation;
@property (assign, nonatomic) BOOL isNumerator;
@property (assign, nonatomic) BOOL isDenominator;
@property (assign, nonatomic) BOOL isResult;
@property (strong, nonatomic) FractionalNumber * operand;

@end

@implementation ViewController

@synthesize operandString;
@synthesize expression;
@synthesize isOperation;
@synthesize operand;
@synthesize isDenominator;
@synthesize isNumerator;
@synthesize stringExpression;
@synthesize displayExpression;
@synthesize displayOperandString;
@synthesize isResult;
@synthesize calculatorDisplay;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    /* Initialize the Operation Look Up Tables */
    operationSelectorLookUpTable = @ {@10:@"multiply:", @11:@"divide:", @12:@"add:", @13: @"subtract:"};
    operationStringLookUpTable = @ { @10:@"x", @11:@"÷", @12:@"+", @13:@"-", @18:@"="};
    
    /* isOperation set to false */
    isOperation = NO;
    
    /* isResult set to false */
    isResult = NO;
    
    /* Allocate and Initialize my expression array and initialize with a 0*/
    expression = [[NSMutableArray alloc]init];
    
    /* Initialize operand to 0 */
    operandString = @"0";
    
    displayExpression = [[NSMutableArray alloc]init];
    displayOperandString = @"0";
    
    /* Set Calculator Display to whatever operandString is */
    [calculatorDisplay setText:displayOperandString];
    
    /* Create a operand fraction */
    operand = [[FractionalNumber alloc]init];
    
    /* Initialize numerator and denominator */
    isNumerator = NO;
    isDenominator = NO;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)isNegative:(NSString *) theString
{
    NSRange searchResult = [theString rangeOfString: @"-"];
    if (searchResult.location == NSNotFound)
    {
        return false;
    }
    else
    {
        //theString = [theString stringByReplacingOccurrencesOfString:@"-" withString:@""];
        return true;

    }
}


-(FractionalNumber *)parseWaitingOperand
{
    NSArray * operandComponents = [operandString componentsSeparatedByString:@"/"];

    int wholeNumber;
    int numerator;
    int denominator;
    int newNumerator;
    
    if (operandComponents.count == 1)
    {

        if ([self isNegative:operandComponents[0]])
        {
            wholeNumber = [operandComponents[0] intValue];
        }
        else
        {
            wholeNumber = [operandComponents[0] intValue];
        }
        return [[FractionalNumber alloc]initWithNumerator:wholeNumber denominator:1];
    }
    else if (operandComponents.count == 2)
    {
        if ([self isNegative:operandComponents[0]])
        {
            numerator = [operandComponents[0] intValue];
        }
        else
        {
            numerator = [operandComponents[0] intValue];
        }

        denominator = [operandComponents[1] intValue];
        return [[FractionalNumber alloc]initWithNumerator:numerator denominator:denominator];
    }
    else
    {
        if ([self isNegative:operandComponents[0]])
        {
            wholeNumber = [operandComponents[0] intValue] * -1;
            numerator = [operandComponents[1] intValue];
            denominator = [operandComponents[2] intValue];
            newNumerator = -1 * ((wholeNumber * denominator)+ numerator);
        }
        else
        {
            wholeNumber = [operandComponents[0] intValue];
            numerator = [operandComponents[1] intValue];
            denominator = [operandComponents[2] intValue];
            newNumerator = (wholeNumber * denominator)+ numerator;
        }
        
        return [[FractionalNumber alloc]initWithNumerator:newNumerator denominator:denominator];
        
    }
}

-(void)redrawLabel
{
    NSString * thisString = @"";
    for (id item in displayExpression)
    {
        thisString = [thisString stringByAppendingString:item];
        thisString = [thisString stringByAppendingString:@" "];
        NSLog (@"thisString: %@", thisString);
    }
    thisString = [thisString stringByAppendingString:displayOperandString];
    NSLog (@"thisString: %@", thisString);
    calculatorDisplay.text = thisString;
}

- (IBAction)buttonPressed:(id)sender {
    
    switch ([sender tag]) {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
            /* Set isOperation to false */
            isOperation = NO;
            
            /* If operand string is 0*/
            if ([operandString isEqualToString:@"0"] || isResult == YES)
            {
                isResult = NO;
                operandString = @"";
                displayOperandString = @"";
                operandString = [operandString stringByAppendingFormat:@"%ld", (long)[sender tag]];
                //((CalculatorDisplay *)self.view).displayOperandString = [((CalculatorDisplay *)self.view).displayOperandString stringByAppendingFormat:@"%ld", (long)[sender tag]];
                displayOperandString = [displayOperandString stringByAppendingFormat:@"%ld", (long)[sender tag]];
            }
            else{
                operandString = [operandString stringByAppendingFormat:@"%ld", (long)[sender tag]];
                //((CalculatorDisplay *)self.view).displayOperandString = [((CalculatorDisplay *)self.view).displayOperandString stringByAppendingFormat:@"%ld", (long)[sender tag]];
                displayOperandString = [displayOperandString stringByAppendingFormat:@"%ld", (long)[sender tag]];
            }
            
            if (isDenominator == YES)
            {
                isDenominator = NO;
            }
            
            /* [redraw] */
            [self redrawLabel];
            
            break;
            /* Binary operations */
        case 10:
        case 11:
        case 12:
        case 13:
            if (isResult == YES)
            {
                isResult = NO;
            }
            if (isOperation == NO && (isNumerator == NO && isDenominator == NO) && ![operandString  isEqual: @"-"])
            {
                Operator * tempOperator = [[Operator alloc]init];
                isOperation = YES;
                
                /* Parse waiting operand */
                operand = [self parseWaitingOperand];
                
                /* attach operand to expression */
                [expression addObject:operand];
                operandString = @"";
                
                /*attach displayOperand to displayExpression */
                //[((CalculatorDisplay *)self.view).displayExpression addObject:((CalculatorDisplay *)self.view).displayOperandString];
                [displayExpression addObject:displayOperandString];
                displayOperandString = @"";
                
                /* create operation object */
                tempOperator.precedence = [sender tag];
                NSLog(@"Tag: %ld", tempOperator.precedence);
                tempOperator.arithmeticOperation = NSSelectorFromString([operationSelectorLookUpTable objectForKey:[NSNumber numberWithLong:[sender tag]]]);
                
                /* Add operation */
                [expression addObject:tempOperator];
                
                /* Add operation to displayExpression */
                //[((CalculatorDisplay *)self.view).displayExpression addObject:[operationStringLookUpTable objectForKey:[NSNumber numberWithLong:[sender tag]]]];
                [displayExpression addObject:[operationStringLookUpTable objectForKey:[NSNumber numberWithLong:[sender tag]]]];
                
                /* [redraw] */
                [self redrawLabel];
                
            }
            break;
        case 14:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"√ operation not available on this version. Please try another operation" delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil];
            [alert show];
        }
            break;
        case 15:
        {
            NSRange searchResult = [operandString rangeOfString: @"-"];
            if (searchResult.location == NSNotFound)
            {
                if ([operandString isEqualToString:@"0"])
                {
                    operandString = @"";
                    displayOperandString = @"";
                }
                operandString = [NSString stringWithFormat:@"-%@", operandString];
                //((CalculatorDisplay *)self.view).displayOperandString = [NSString stringWithFormat:@"-%@", ((CalculatorDisplay *)self.view).displayOperandString];
                displayOperandString = [NSString stringWithFormat:@"-%@", displayOperandString];
            }
            else
            {
                operandString = [operandString stringByReplacingOccurrencesOfString:@"-" withString:@""];
                //[((CalculatorDisplay *)self.view).displayOperandString stringByReplacingOccurrencesOfString:@"-" withString:@""];
                displayOperandString = [displayOperandString stringByReplacingOccurrencesOfString:@"-" withString:@""];
            }
            [self redrawLabel];
            
        }
            break;
        case 16:
            
            if (isNumerator == NO)
            {
                isNumerator = YES;
                if ([operandString isEqualToString:@"0"])
                {
                        operandString = @"";
                        displayOperandString = @""; 
                }
                if (operandString.length != 0)
                {
                    operandString = [operandString stringByAppendingString:@"/"];
                    //((CalculatorDisplay *)self.view).displayOperandString = [((CalculatorDisplay *)self.view).displayOperandString stringByAppendingString:@"/"];
                    if ([operandString isEqualToString:@"-"])
                    {
                        displayOperandString = [displayOperandString stringByAppendingString:@""];
                    }
                    else{
                        displayOperandString = [displayOperandString stringByAppendingString:@" "];

                    }
                    /* [redraw] */
                    [self redrawLabel];
                }
            }
            break;
            
        case 17:
            if (isNumerator == YES)
            {
                isDenominator = YES;
                isNumerator = NO;
                operandString = [operandString stringByAppendingString:@"/"];
                //((CalculatorDisplay *)self.view).displayOperandString = [((CalculatorDisplay *)self.view).displayOperandString stringByAppendingString:@"/"];
                displayOperandString = [displayOperandString stringByAppendingString:@"/"];
                /* [redraw] */
                [self redrawLabel];
            }
            break;
            /* Equal = sign */
        case 18:
        {
            if (isOperation == NO && (isNumerator == NO && isDenominator == NO)&& ![operandString  isEqual: @"-"])
            {
                if (operandString.length != 0)
                {
                    /* Parse waiting operand */
                    operand = [self parseWaitingOperand];
                    /* attach operand to expression */
                    [expression addObject:operand];
                    operandString = @"";
                    
                    /* append to displayExpression */
                    //[((CalculatorDisplay *)self.view).displayExpression addObject:((CalculatorDisplay *)self.view).displayOperandString];
                    //((CalculatorDisplay *)self.view).displayOperandString = @"";
                    [displayExpression addObject:displayOperandString];
                    displayOperandString = @"";
                    
                    /* Add operation to displayExpression */
                    //[((CalculatorDisplay *)self.view).displayExpression addObject:[operationStringLookUpTable objectForKey:[NSNumber numberWithLong:[sender tag]]]];
                    [displayExpression addObject:[operationStringLookUpTable objectForKey:[NSNumber numberWithLong:[sender tag]]]];
                }
                
                
                /* Print out the expression Array */
                for (id object in expression)
                {
                    if ([object isKindOfClass:[NSString class]])
                    {
                        NSLog (@"Number: %@", object);
                    }
                    else{
                        NSLog (@"%@", [object description]);
                    }
                }
            }
            
            /* Call the calculator brain */
            CalculatorBrain * brain = [CalculatorBrain calculatorBrain];
            id<ArithmeticProtocol, NSObject> result = [brain evaluate:expression] ;
            
            NSLog (@"Result: %@", result);
            
            if ([[result stringRepresentationOfFraction] containsString:@"0/1"])
            {
                [displayExpression addObject:@"0"];
            }
            else if ([[result stringRepresentationOfFraction] containsString:@"0/0"])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Invalid operation" delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil];
                [alert show];
            }
            else
            {
            /* Append the result to the displayExpression */
            //[((CalculatorDisplay *)self.view).displayExpression addObject:[result stringRepresentationOfFraction]];
                [displayExpression addObject:[result stringRepresentationOfFraction]];
            }
            
            /* [redraw] */
            [self redrawLabel];
            
            /* Reset expression and displayExpression with only the result */
            [expression removeAllObjects];
            //[((CalculatorDisplay *)self.view).displayExpression removeAllObjects];
            [displayExpression removeAllObjects];
            
            /* Make operandString equal to the result */
            if ([[result stringRepresentationOfFraction] containsString:@"0/1"])
            {
                operandString = @"0";
            }
            else if ([[result stringRepresentationOfFraction] containsString:@"0/0"])
            {
                operandString = @"0";
 
            }
            else
            {
                operandString = [result stringRepresentationOfFraction];
            }
            //((CalculatorDisplay *)self.view).displayOperandString = operandString;
            displayOperandString = operandString;
            
            /* set isResult to true */
            isResult = YES;
            
            break;
        }
        case 19:
            /* Reset everything!  */
            [expression removeAllObjects];
            //[((CalculatorDisplay *)self.view).displayExpression removeAllObjects];
            [displayExpression removeAllObjects];
            //((CalculatorDisplay *)self.view).displayOperandString = @"0";
            displayOperandString = @"0";
            operandString = @"0";
            isNumerator = NO;
            isDenominator = NO;
            isOperation = NO;
            isResult = NO;
            
            /* [redraw] */
            [self redrawLabel];
            
            break;
        default:
            break;
    }
    
    
    
}
@end

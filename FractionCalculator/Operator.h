/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is reponsible for defining the an operator
 */

#import <Foundation/Foundation.h>

@interface Operator : NSObject

@property (assign, nonatomic) unsigned long int precedence;
@property (assign, nonatomic) SEL arithmeticOperation;

@end

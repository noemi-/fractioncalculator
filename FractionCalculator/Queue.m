/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is reponsible for implementing a queue
 */

#import "Queue.h"

@implementation Queue

@synthesize aQueue;

-(id)init
{
    self = [super init];
    if (self)
    {
        aQueue = [[NSMutableArray alloc]init];
    }
    
    return self;
}

-(unsigned long)count{
    return aQueue.count;
}

-(void)enqueue:(id)item
{
    if (item != nil)
    {
        [aQueue addObject:item];
    }
}

-(id)dequeue
{
    if (aQueue.count > 0)
    {
        id DequeuedItem = [aQueue objectAtIndex:0];
        [aQueue removeObjectAtIndex:0];
        return DequeuedItem;
        
    }
    return nil;
}

-(id)top
{
    if (aQueue.count > 0)
    {
        return [aQueue objectAtIndex:0];
    }
    else
        return nil;
}

-(BOOL)isEmpty
{
    return aQueue.count == 0;
}

-(NSMutableArray *)morphToArray
{
    NSMutableArray * tempArray = [[NSMutableArray alloc]init];
    while (!self.isEmpty)
    {
        [tempArray addObject:[self dequeue]];
    }
    return tempArray;
}


@end

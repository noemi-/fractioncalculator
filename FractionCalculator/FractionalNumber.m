/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is responsible for implementing the FractionalNumber object that does arithmetic operations with fractions
 */

#import "FractionalNumber.h"

@implementation FractionalNumber

@synthesize numerator;
@synthesize denominator;


-(NSString * )description
{
    return [NSString stringWithFormat:@"Numerator %d Denominator %d ", self.numerator, self.denominator];
}


-(NSString *)stringRepresentationOfFraction
{
    /* Make sure only numerator has negative sign */
    if ((self.numerator < 0 && self.denominator < 0) || self.denominator < 0)
    {
        self.numerator *= -1;
        self.denominator *= -1;
    }
    
    /* If its equal to 1 */
    if (abs([self numerator]) == abs([self denominator]) && [self numerator] != 0 && [self denominator] != 0)
    {
        return [NSString stringWithFormat:@"%d", (self.numerator/self.denominator)]; 
    }
    /* If it will be a mixed fraction */
    if (abs([self numerator]) > abs([self denominator]))
    {
        int wholeNumber = self.numerator/ self.denominator;
        NSLog (@"this is wholeNumber: %d", wholeNumber);
        FractionalNumber * remainder= [[FractionalNumber alloc]initWithNumerator:(abs(self.numerator)%abs(self.denominator)) denominator:(self.denominator)];
        [remainder description];
        [remainder reduceFraction];
        //return [NSString stringWithFormat:@"%d/%d/%d", wholeNumber, remainder.numerator,remainder.denominator ];
        return [NSString stringWithFormat:@"%d %d/%d", wholeNumber, remainder.numerator,remainder.denominator ];
    }
    /* If just fraction */
    else{
        return [NSString stringWithFormat:@"%d/%d", [self numerator], [self denominator]];
    }

}

-(FractionalNumber *)initWithNumerator:(int)aNumerator denominator:(int)aDenominator
{
    self = [super init];
    if (self)
    {
        numerator = aNumerator;
        denominator = aDenominator;
    }
    return self;
}

-(FractionalNumber *)divide:(FractionalNumber *)rightHandSide
{
    /* If the denominator equals to 0 just return a 0/0 fraction which will be an error */
    if ( self.denominator == 0 || rightHandSide.numerator == 0)
    {
        return [[FractionalNumber alloc]initWithNumerator:0 denominator:0];
    }
    else{
        /* See if the fractions can be reduced */
        [self reduceFraction];
        [rightHandSide reduceFraction];
        
        /* Flip the rightHandSide fraction */
        int temp = rightHandSide.denominator;
        rightHandSide.denominator = rightHandSide.numerator;
        rightHandSide.numerator = temp;
        
        /* See if we can cross reduce fractions */
        [self crossReduce:rightHandSide];
        
        
        FractionalNumber * resultFraction = [[FractionalNumber alloc]initWithNumerator:(self.numerator * rightHandSide.numerator) denominator:(self.denominator * rightHandSide.denominator)];
        
        [resultFraction reduceFraction];
        
        return resultFraction;
    }
}

-(FractionalNumber *)multiply:(FractionalNumber *)rightHandSide
{
    
    /* If the denominator equals to 0 just return a 0/0 fraction which will be an error */
    if ( self.denominator == 0 || rightHandSide.denominator == 0)
    {
        return [[FractionalNumber alloc]initWithNumerator:0 denominator:0];
    }
    else{
        /* See if the fractions can be reduced */
        [self reduceFraction];
        [rightHandSide reduceFraction];
        
        /* See if we can cross reduce fractions */
        [self crossReduce:rightHandSide];
        
        
        FractionalNumber * resultFraction = [[FractionalNumber alloc]initWithNumerator:(self.numerator * rightHandSide.numerator) denominator:(self.denominator * rightHandSide.denominator)];
        
        [resultFraction reduceFraction];
        
        return resultFraction;
    }
}

-(FractionalNumber *)add:(FractionalNumber *)rightHandSide
{
    /* If the denominator equals to 0 just return a 0/0 fraction which will be an error */
    if ( self.denominator == 0 || rightHandSide.denominator == 0)
    {
        return [[FractionalNumber alloc]initWithNumerator:0 denominator:0];
    }
    else
    /* If the denomininators are the same just add through*/
    {
        if (self.denominator == rightHandSide.denominator)
        {

            return [[FractionalNumber alloc]initWithNumerator:(self.numerator + rightHandSide.numerator) denominator:self.denominator];
        }
        else{
            /* Get the lcd of both denominators */
            int lcd = [FractionalNumber leastCommonMultiple:self.denominator and:rightHandSide.denominator];
            
            
            
            /* NSLog(@"LCD: %d", lcd);
            NSLog(@"Left Denominator: %d", self.denominator);
            NSLog(@"Right Denominator: %d", rightHandSide.denominator); */
            int leftNumber = lcd/self.denominator;
            /* NSLog(@"LeftNumber: %d", leftNumber); */
            int rightNumber = lcd/rightHandSide.denominator;
            /* NSLog(@"RightNumber: %d", rightNumber); */
            

            
            int resultNumerator = (self.numerator * leftNumber) + (rightHandSide.numerator * rightNumber);
            /* NSLog(@"resultNumerator: %d", resultNumerator); */
            int resultDenominator = lcd;
            /* NSLog(@"resultDenominator: %d", resultDenominator); */
            
            FractionalNumber * resultFraction =[[FractionalNumber alloc]initWithNumerator:resultNumerator denominator:resultDenominator];
            [resultFraction reduceFraction];
            
            return resultFraction;
        }
    }
}

-(FractionalNumber * )subtract:(FractionalNumber *)rightHandSide
{
    /* If the denominator equals to 0 just return a 0/0 fraction which will be an error */
    if ( self.denominator == 0 || rightHandSide.denominator == 0)
    {
        return [[FractionalNumber alloc]initWithNumerator:0 denominator:0];
    }
    else
    /* If the denomininators are the same just subtract through*/
    {
        if (self.denominator == rightHandSide.denominator)
        {
            return [[FractionalNumber alloc]initWithNumerator:(self.numerator - rightHandSide.numerator) denominator:self.denominator];
        }
        else{
            /* Get the lcd of both denominators */
            int lcd = [FractionalNumber leastCommonMultiple:self.denominator and:rightHandSide.denominator];

            /* NSLog(@"LCD: %d", lcd);
            NSLog(@"Left Denominator: %d", self.denominator);
            NSLog(@"Right Denominator: %d", rightHandSide.denominator);*/
            int leftNumber = lcd/self.denominator;
            /* NSLog(@"LeftNumber: %d", leftNumber); */
            int rightNumber = lcd/rightHandSide.denominator;
            /* NSLog(@"RightNumber: %d", rightNumber); */
            int resultNumerator = (self.numerator * leftNumber) - (rightHandSide.numerator * rightNumber);
            /* NSLog(@"resultNumerator: %d", resultNumerator); */
            int resultDenominator = lcd;
            /* NSLog(@"resultDenominator: %d", resultDenominator); */
            
            FractionalNumber * resultFraction =[[FractionalNumber alloc]initWithNumerator:resultNumerator denominator:resultDenominator];
            [resultFraction reduceFraction];
            
            return resultFraction;
        }
    }
}

/* Utility Functions */

-(void) crossReduce:(FractionalNumber *)rightHandside
{
    int gcdLeftToRight;
    int gcdRightToLeft;
    
    if (self.denominator >= rightHandside.numerator)
    {
        gcdLeftToRight  = [FractionalNumber greatestCommonDivisor:self.denominator and:rightHandside.numerator];
        /* NSLog(@"gcd: %d", gcdLeftToRight); */
    }
    else{
        gcdLeftToRight = [FractionalNumber greatestCommonDivisor:rightHandside.numerator and:self.denominator];
       /*  NSLog(@"gcd: %d", gcdLeftToRight); */
    }
    
    if (rightHandside.denominator >= self.numerator)
    {
        gcdRightToLeft  = [FractionalNumber greatestCommonDivisor:rightHandside.denominator and:self.numerator];
       /*  NSLog(@"gcd: %d", gcdRightToLeft); */
    }
    else{
        gcdRightToLeft = [FractionalNumber greatestCommonDivisor:self.numerator and:rightHandside.denominator];
        /* NSLog(@"gcd: %d", gcdRightToLeft); */
    }
    
    self.denominator /= gcdLeftToRight;
    rightHandside.numerator /=gcdLeftToRight;
    self.numerator /=gcdRightToLeft;
    rightHandside.denominator /=gcdRightToLeft;
}


-(FractionalNumber *)plusOrMinus
{
    return self; 
}

-(void)reduceFraction
{
    int gcd;
    if (self.denominator >= self.numerator)
    {
        gcd  = [FractionalNumber greatestCommonDivisor:self.denominator and:self.numerator];
        /* NSLog(@"gcd: %d", gcd); */
    }
    else{
        gcd = [FractionalNumber greatestCommonDivisor:self.numerator and:self.denominator];
        /* NSLog(@"gcd: %d", gcd); */
    }
    
    self.denominator /=  gcd;
    /* NSLog(@"resultDenominator: %d", self.denominator); */
    self.numerator /= gcd;
    /* NSLog(@"resultNumerator: %d", self.numerator); */
}


+(int)greatestCommonDivisor:(int)firstNumber and:(int)secondNumber
{
    if (secondNumber == 0)
    {
        return firstNumber;
    }
    else
    {
        return [FractionalNumber greatestCommonDivisor:secondNumber and:(firstNumber % secondNumber)];
    }
    
}

+(int)leastCommonMultiple:(int)firstNumber and:(int)secondNumber
{

    return (abs(firstNumber * secondNumber)/ [FractionalNumber greatestCommonDivisor:firstNumber and:secondNumber]);
}




@end
